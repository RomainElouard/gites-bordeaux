import { Contact } from "./interfaces.manager";

const nam = {
  name: "Nam",
  tel: "+33628616885",
  email: "gites-tresses@gitesdebordeaux.fr",
} as Contact;
const didier = {
  name: "Didier",
  tel: "+33627146083",
  email: "gites-bassin@gitesdebordeaux.fr",
} as Contact;
const romain = {
  name: "Romain",
  tel: "+33786392344",
  email: "gites-medoc@gitesdebordeaux.fr",
} as Contact;

export { nam, didier, romain };
