export type ContactName = "Nam" | "Didier" | "Romain";
export type ContactTel = "+33628616885" | "+33627146083" | "+33786392344";

export interface DescriptionCard {
  title: string;
  text: string;
  contact?: Contact;
}

export interface Contact {
  name: ContactName;
  tel: ContactTel;
}

export interface Picture {
  src: string;
  alt: string;
  description?: string;
}

export interface GalleryPictures {
  title: string;
  pictures: Picture[];
}
