import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge1 = {
  title: "Gîte 1",
  pictures: [
    {
      src: "/pictures/tresses/lodge-1/bathroom-1.jpg",
      alt: "image salon 1",
    },
    {
      src: "/pictures/tresses/lodge-1/bathroom-2.jpg",
      alt: "image salon 2",
    },
    {
      src: "/pictures/tresses/lodge-1/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/tresses/lodge-1/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/lodge-1/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/lodge-1/corridor-1.jpg",
      alt: "image corridor 1",
    },
    {
      src: "/pictures/tresses/lodge-1/corridor-2.jpg",
      alt: "image corridor 2",
    },
    {
      src: "/pictures/tresses/lodge-1/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/tresses/lodge-1/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/tresses/lodge-1/kitchen-3.jpg",
      alt: "image kitchen 3",
    },
    {
      src: "/pictures/tresses/lodge-1/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/lodge-1/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/tresses/lodge-1/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/tresses/lodge-1/terrace-1.jpg",
      alt: "image terrace 2",
    },
    {
      src: "/pictures/tresses/lodge-1/terrace-2.jpg",
      alt: "image terrace 3",
    },
    {
      src: "/pictures/tresses/lodge-1/toilet-1.jpg",
      alt: "image toilet 1",
    },
  ],
} as GalleryPictures;

export { lodge1 };
