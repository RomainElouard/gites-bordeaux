import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge4 = {
  title: "Gîte 4",
  pictures: [
    {
      src: "/pictures/tresses/lodge-4/bathroom-1.jpg",
      alt: "image bathroom 1",
    },
    {
      src: "/pictures/tresses/lodge-4/bathroom-2.jpg",
      alt: "image bathroom 2",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-4.jpg",
      alt: "image bedroom 4",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-5.jpg",
      alt: "image bedroom 5",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-6.jpg",
      alt: "image bedroom 6",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-7.jpg",
      alt: "image bedroom 7",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-8.jpg",
      alt: "image bedroom 8",
    },
    {
      src: "/pictures/tresses/lodge-4/bedroom-9.jpg",
      alt: "image bedroom 9",
    },
    {
      src: "/pictures/tresses/lodge-4/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/tresses/lodge-4/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/lodge-4/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/tresses/lodge-4/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/tresses/lodge-4/terrace-1.jpg",
      alt: "image terrace 1",
    },
  ],
} as GalleryPictures;

export { lodge4 };
