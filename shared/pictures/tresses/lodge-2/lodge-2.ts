import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge2 = {
  title: "Gîte 2",
  pictures: [
    {
      src: "/pictures/tresses/lodge-2/bathroom-1.jpg",
      alt: "image bathroom 1",
    },
    {
      src: "/pictures/tresses/lodge-2/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/tresses/lodge-2/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/lodge-2/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/lodge-2/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/lodge-2/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/tresses/lodge-2/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/tresses/lodge-2/living-room-4.jpg",
      alt: "image living-room 4",
    },
    {
      src: "/pictures/tresses/lodge-2/living-room-5.jpg",
      alt: "image living-room 5",
    },
    {
      src: "/pictures/tresses/lodge-2/living-room-6.jpg",
      alt: "image living-room 6",
    },
    {
      src: "/pictures/tresses/lodge-2/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/tresses/lodge-2/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/tresses/lodge-2/terrace-1.jpg",
      alt: "image terrace 1",
    },
    {
      src: "/pictures/tresses/lodge-2/terrace-2.jpg",
      alt: "image terrace 2",
    },
  ],
} as GalleryPictures;

export { lodge2 };
