import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge5 = {
  title: "Gîte 5",
  pictures: [
    {
      src: "/pictures/tresses/lodge-5/bathroom-1.jpg",
      alt: "image bathroom 1",
    },
    {
      src: "/pictures/tresses/lodge-5/bathroom-2.jpg",
      alt: "image bathroom 2",
    },
    {
      src: "/pictures/tresses/lodge-5/bathroom-3.jpg",
      alt: "image bathroom 3",
    },
    {
      src: "/pictures/tresses/lodge-5/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/tresses/lodge-5/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/lodge-5/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/lodge-5/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/tresses/lodge-5/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/tresses/lodge-5/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/lodge-5/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/tresses/lodge-5/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/tresses/lodge-5/terrace-1.jpg",
      alt: "image terrace 1",
    },
  ],
} as GalleryPictures;

export { lodge5 };
