import { GalleryPictures } from "@/shared/interfaces.manager";

const garden = {
  title: "Espace partagé",
  pictures: [
    {
      src: "/pictures/tresses/shared/garden-1.jpg",
      alt: "image garden 1",
    },
    {
      src: "/pictures/tresses/shared/garden-2.jpg",
      alt: "image garden 2",
    },
    {
      src: "/pictures/tresses/shared/garden-3.jpg",
      alt: "image garden 3",
    },
    {
      src: "/pictures/tresses/shared/garden-4.jpg",
      alt: "image garden 4",
    },
    {
      src: "/pictures/tresses/shared/garden-5.jpg",
      alt: "image garden 5",
    },
    {
      src: "/pictures/tresses/shared/garden-6.jpg",
      alt: "image garden 6",
    },
    {
      src: "/pictures/tresses/shared/garden-7.jpg",
      alt: "image garden 7",
    },
  ],
} as GalleryPictures;

export { garden };
