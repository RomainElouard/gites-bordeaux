import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge3 = {
  title: "Gîte 3",
  pictures: [
    {
      src: "/pictures/tresses/lodge-3/bathroom-1.jpg",
      alt: "image bathroom 1",
    },
    {
      src: "/pictures/tresses/lodge-3/bathroom-2.jpg",
      alt: "image bathroom 2",
    },
    {
      src: "/pictures/tresses/lodge-3/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/tresses/lodge-3/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/lodge-3/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/lodge-3/bedroom-4.jpg",
      alt: "image bedroom 4",
    },
    {
      src: "/pictures/tresses/lodge-3/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/lodge-3/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/tresses/lodge-3/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/tresses/lodge-3/living-room-4.jpg",
      alt: "image living-room 4",
    },
    {
      src: "/pictures/tresses/lodge-3/terrace-1.jpg",
      alt: "image terrace 1",
    },
  ],
} as GalleryPictures;

export { lodge3 };
