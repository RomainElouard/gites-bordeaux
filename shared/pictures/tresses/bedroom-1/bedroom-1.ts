import { GalleryPictures } from "@/shared/interfaces.manager";

const bedroom1 = {
  title: "Chambre",
  pictures: [
    {
      src: "/pictures/tresses/bedroom-1/bedroom-1.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/bedroom-1/bedroom-2.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/bedroom-1/garden-1.jpg",
      alt: "image garden 1",
    },
    {
      src: "/pictures/tresses/bedroom-1/garden-2.jpg",
      alt: "image garden 2",
    },
    {
      src: "/pictures/tresses/bedroom-1/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/tresses/bedroom-1/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/tresses/bedroom-1/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/bedroom-1/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/tresses/bedroom-1/living-room-3.jpg",
      alt: "image living-room 3",
    },
  ],
} as GalleryPictures;

export { bedroom1 };
