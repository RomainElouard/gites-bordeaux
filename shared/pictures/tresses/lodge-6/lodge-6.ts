import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge6 = {
  title: "Gîte 6",
  pictures: [
    {
      src: "/pictures/tresses/lodge-6/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/tresses/lodge-6/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/tresses/lodge-6/bedroom-4.jpg",
      alt: "image bedroom 4",
    },
    {
      src: "/pictures/tresses/lodge-6/garden-1.jpg",
      alt: "image garden 1",
    },
    {
      src: "/pictures/tresses/lodge-6/garden-2.jpg",
      alt: "image garden 2",
    },
    {
      src: "/pictures/tresses/lodge-6/garden-3.jpg",
      alt: "image garden 3",
    },
    {
      src: "/pictures/tresses/lodge-6/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/tresses/lodge-6/toilet-1.jpg",
      alt: "image toilet 1",
    },
    {
      src: "/pictures/tresses/lodge-6/upstairs-1.jpg",
      alt: "image upstairs 1",
    },
  ],
} as GalleryPictures;

export { lodge6 };
