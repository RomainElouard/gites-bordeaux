import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge1 = {
  title: "Gîte 1",
  pictures: [
    {
      src: "/pictures/sainte-helene/lodge-1/terrace-1.jpg",
      alt: "image terrace 1",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/living-room-4.jpg",
      alt: "image living-room 4",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/bedroom-4.jpg",
      alt: "image bedroom 4",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/kitchen-3.jpg",
      alt: "image kitchen 3",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/living-room-2.jpg",
      alt: "image living-room 2",
    },

    {
      src: "/pictures/sainte-helene/lodge-1/living-room-5.jpg",
      alt: "image living-room 5",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/toilet-1.jpg",
      alt: "image toilet 1",
    },
    {
      src: "/pictures/sainte-helene/lodge-1/toilet-2.jpg",
      alt: "image toilet 2",
    },

    {
      src: "/pictures/sainte-helene/lodge-1/terrace-2.jpg",
      alt: "image terrace 2",
    },
  ],
} as GalleryPictures;

export { lodge1 };
