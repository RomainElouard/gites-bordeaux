import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge2 = {
  title: "Gîte 2",
  pictures: [
    {
      src: "/pictures/gujan-mestras/lodge-2/bathroom-1.jpg",
      alt: "image bathroom 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bathroom-2.jpg",
      alt: "image bathroom 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bathroom-3.jpg",
      alt: "image bathroom 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bathroom-4.jpg",
      alt: "image bathroom 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bathroom-5.jpg",
      alt: "image bathroom 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-4.jpg",
      alt: "image bedroom 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-5.jpg",
      alt: "image bedroom 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-6.jpg",
      alt: "image bedroom 6",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/bedroom-7.jpg",
      alt: "image bedroom 7",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/corridor-1.jpg",
      alt: "image corridor 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/garden-1.jpg",
      alt: "image garden 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/garden-2.jpg",
      alt: "image garden 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/garden-3.jpg",
      alt: "image garden 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-4.jpg",
      alt: "image living-room 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-5.jpg",
      alt: "image living-room 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-6.jpg",
      alt: "image living-room 6",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-7.jpg",
      alt: "image living-room 7",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/living-room-8.jpg",
      alt: "image living-room 8",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/toilet-1.jpg",
      alt: "image toilet 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/toilet-2.jpg",
      alt: "image toilet 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-2/upstairs-1.jpg",
      alt: "image upstairs 1",
    },
  ],
} as GalleryPictures;

export { lodge2 };
