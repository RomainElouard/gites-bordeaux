import { GalleryPictures } from "@/shared/interfaces.manager";

const lodge1 = {
  title: "Gîte 1",
  pictures: [
    {
      src: "/pictures/gujan-mestras/lodge-1/bathroom-1.jpg",
      alt: "image bathroom 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bathroom-2.jpg",
      alt: "image bathroom 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/beach-1.jpg",
      alt: "image beach 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/beach-2.jpg",
      alt: "image beach 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/beach-3.jpg",
      alt: "image beach 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-1.jpg",
      alt: "image bedroom 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-2.jpg",
      alt: "image bedroom 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-3.jpg",
      alt: "image bedroom 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-4.jpg",
      alt: "image bedroom 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-5.jpg",
      alt: "image bedroom 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-6.jpg",
      alt: "image bedroom 6",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-7.jpg",
      alt: "image bedroom 7",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-8.jpg",
      alt: "image bedroom 8",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-9.jpg",
      alt: "image bedroom 9",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-10.jpg",
      alt: "image bedroom 10",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-11.jpg",
      alt: "image bedroom 11",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/bedroom-12.jpg",
      alt: "image bedroom 12",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/corridor-1.jpg",
      alt: "image corridor 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/garden-1.jpg",
      alt: "image garden 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/garden-2.jpg",
      alt: "image garden 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/garden-3.jpg",
      alt: "image garden 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/garden-4.jpg",
      alt: "image garden 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/garden-5.jpg",
      alt: "image garden 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/garden-6.jpg",
      alt: "image garden 6",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/kitchen-1.jpg",
      alt: "image kitchen 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/kitchen-2.jpg",
      alt: "image kitchen 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/kitchen-3.jpg",
      alt: "image kitchen 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/kitchen-4.jpg",
      alt: "image kitchen 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/kitchen-5.jpg",
      alt: "image kitchen 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/kitchen-6.jpg",
      alt: "image kitchen 6",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/living-room-1.jpg",
      alt: "image living-room 1",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/living-room-2.jpg",
      alt: "image living-room 2",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/living-room-3.jpg",
      alt: "image living-room 3",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/living-room-4.jpg",
      alt: "image living-room 4",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/living-room-5.jpg",
      alt: "image living-room 5",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/living-room-6.jpg",
      alt: "image living-room 6",
    },
    {
      src: "/pictures/gujan-mestras/lodge-1/toilet-1.jpg",
      alt: "image toilet 1",
    },
  ],
} as GalleryPictures;

export { lodge1 };
